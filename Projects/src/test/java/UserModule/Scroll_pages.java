package UserModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Scroll_pages {

    public WebDriver driver;



    @BeforeTest
    public void setup() {
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        this.driver.manage().window().maximize();
    }

    @Test
    public void scrollHomePage() throws AWTException, InterruptedException {
        driver.get("https://www.amissible.com:444/#/home");
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        //jse.executeScript("window.scrollBy(0,1519)");
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_END);
        driver.findElement(By.xpath("//*[@id=\"services\"]/app-scroll-to-top/div/button")).click();
    }

}
