package UserModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Get_quote {
    public WebDriver driver;
    WebDriverWait driverWait ;

    @BeforeTest
    public void setup() {
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        this.driver.manage().window().maximize();
    }

    @Test(description = "open website and test get quote")
    public void testHome() throws InterruptedException {
        //Open home page
        this.driver.get("https://www.amissible.com:444/#/home");
        System.out.println("Website opened");

        driverWait = new WebDriverWait(driver,20);

        //Step 1 to get quote
        driver.findElement(By.xpath("//*[@id=\"Moving From\"]")).sendKeys("546578");
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"Moving To\"]")));
        driver.findElement(By.xpath("//*[@id=\"Moving To\"]")).sendKeys("546598");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"banner\"]/div/div/div[2]/div/app-form/div[1]/form/button")).click();

        //Step 2 to get quote
        driver.findElement(By.id("selectOption")).click();
        {
            WebElement dropdown = driver.findElement(By.id("selectOption"));
            dropdown.findElement(By.xpath("//option[. = 'Studio']")).click();
        }
        driver.findElement(By.id("selectOption")).click();
        Thread.sleep(1500);
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("document.getElementById('movingDate').value='2021-07-17'");
        driver.findElement(By.cssSelector(".form-group:nth-child(3) > #selectOption")).click();
        {
            WebElement dropdown = driver.findElement(By.cssSelector(".form-group:nth-child(3) > #selectOption"));
            dropdown.findElement(By.xpath("//option[. = 'Asia/Bangkok']")).click();
        }
        driver.findElement(By.cssSelector(".form-group:nth-child(3) > #selectOption")).click();
        driver.findElement(By.name("goNext")).click();

        //Step 3 to get quote
    }
}
