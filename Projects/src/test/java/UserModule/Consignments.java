package UserModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Consignments {

    public WebDriver driver;

    @BeforeTest
    public void setup() {
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        this.driver.manage().window().maximize();
    }

    @Test(description = "Testing of consignments in user module")
    public void testConsignment() throws InterruptedException {
        //login
        this.driver.get("https://www.amissible.com:444/#/home");
        this.driver.findElement(By.xpath("//*[@id=\"navbarCollapse\"]/div/a[6]")).click();
        Thread.sleep(1300);
        WebElement email = this.driver.findElement(By.name("email"));
        email.sendKeys(new CharSequence[]{"liv@gmail.com"});
        Thread.sleep(1300);
        driver.findElement(By.name("password")).sendKeys(new CharSequence[]{"941984"});
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/app-root/div/app-login/div[1]/form/input")).click();
        //driver.findElement(By.xpath("//*[@id='myModal']/div/div/div[2]/button")).click();
        Thread.sleep(1300);
        driver.findElement(By.xpath("//button[@tabindex='0']")).click();
        driver.navigate().forward();
        driver.navigate().refresh();
        Thread.sleep(1300);
        //See Contract
        driver.findElement(By.xpath("//*[@id='ag-39-input']")).click();
        Thread.sleep(1300);
        driver.findElement(By.xpath("//div[@name='getContract']")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id='form']/div/div[5]/div/form/div/div/img")).click();
        WebElement canvas = driver.findElement(By.xpath("//*[@id='form']/div/div[5]/div/form/div/div/img"));
        Actions builder = new Actions(driver);
        System.out.println(canvas.getSize());
        System.out.println(canvas.getLocation());
        builder.moveToElement(canvas, 10, 7);
        builder.click();
        builder.perform();


    }

}
