package TestNg;

import org.testng.annotations.*;

public class TestNGClass {

    /*
        Setup system property for chrome
        Launch Chrome Browser
        Login Method
        Enter the url
        Google Title Test
        Logout
        Enter the url
        Search something
        Logout
        Close Browser
        Delete All Cookies
        Generate Test Report
     */

    //Pre-Condition annotations-- Starting with @Before
    @BeforeSuite
    public void setup(){
        System.out.println("Setup system property for chrome");
    }

    @BeforeTest
    public void launchBrowser(){
        System.out.println("Launch Chrome Browser");
    }

    @BeforeClass
    public void login(){
        System.out.println("Login Method");
    }

    @BeforeMethod
    public void enterUrl(){
        System.out.println("Enter the url");
    }

    //Test Cases -- Starting with @Test
    @Test
    public void googleTitleTest(){
        System.out.println("Google Title Test");
    }

    @Test
    public void search(){
        System.out.println("Search something");
    }

    //Post- Conditions--Starting with @After
    @AfterMethod
    public void logout(){
        System.out.println("Logout");
    }

    @AfterClass
    public void closeBrowser(){
        System.out.println("Close Browser");
    }

    @AfterTest
    public void deleteAllCookies(){
        System.out.println("Delete All Cookies");
    }

    @AfterSuite
    public void generateTestReport(){
        System.out.println("Generate Test Report");
    }
}
