package TestNg;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.testng.annotations.Test;

public class Reporter {

    @Test
    public void report_test(){
        ExtentReports extent = new ExtentReports();

        ExtentSparkReporter spark = new ExtentSparkReporter("target/Spark.html");
        extent.attachReporter(spark);

        ExtentTest logger = extent.createTest("LoginTest");
        logger.log(Status.INFO, "Login Test");
        logger.log(Status.PASS, "Title verified");

        extent.flush();

    }
}
