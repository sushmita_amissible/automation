package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class ComplaintRecords {
    WebDriver driver;

    @BeforeTest
    public void test() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority = 1, description = "Login to admin module and open Complaint Records")
    public void complaintReport() throws InterruptedException {
        //Open website
        driver.get("https://www.amissible.com:444/#/home");
        driver.findElement(By.xpath("//a[@href ='http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/']")).click();

        //Login as Admin
        WebElement email = driver.findElement(By.name("email"));
        Thread.sleep(1500);
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        Thread.sleep(1500);
        WebElement login = driver.findElement(By.name("login"));
        login.click();

        //Open consignment's report
        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();
        Thread.sleep(1500);
        driver.findElement(By.linkText("Complaint Report")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 2, description = "Edit complaints")
    public void edit() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-complaint/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[2]/div[1]/div/div/div/div[2]")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/app-root/app-report-complaint/div/button[2]")).click();
        Thread.sleep(1500);

        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[6]/td/select")).click();
        {
            WebElement dropdown = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[6]/td/select/option[2]"));
            dropdown.findElement(By.xpath("//option[. = 'RESOLVING']")).click();
            Thread.sleep(1500);

        }driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[6]/td/select")).click();
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[7]/td/textarea")).clear();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[7]/td/textarea")).
                sendKeys("Resolving Complaint");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[2]")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 3, description = "Export complaints csv file")
    public void exportCSV(){
        driver.findElement(By.xpath("/html/body/app-root/app-report-complaint/div/button[1]")).click();

    }
}
