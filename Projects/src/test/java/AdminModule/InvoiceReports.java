package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class InvoiceReports {
    WebDriver driver;

    @BeforeTest
    public void test() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority = 1,description = "Login as admin and open invoice reports")
    public void invoiceReport() throws InterruptedException {
        //Open website
        driver.get("https://www.amissible.com:444/#/home");
        driver.findElement(By.xpath("//a[@href ='http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/']")).click();

        //Login as Admin
        WebElement email = driver.findElement(By.name("email"));
        Thread.sleep(1500);
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        Thread.sleep(1500);
        WebElement login = driver.findElement(By.name("login"));
        login.click();

        //Open consignment's report
        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();
        Thread.sleep(1500);
        driver.findElement(By.linkText("Invoice Report")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 2, description = "view invoice")
    public void viewInvoice() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-invoice/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[3]/div[1]/div/div/div/div[2]/input")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/app-root/app-report-invoice/div/button[2]")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td/textarea")).
                clear();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td/textarea")).
                sendKeys("Car");
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td/input")).clear();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td/input")).sendKeys("2");
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[3]/td/input")).clear();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[3]/td/input")).sendKeys("6.8");
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[3]/div/div/div/div/div[2]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[2]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/input")).clear();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/input")).sendKeys("4");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1500);
        //driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[5]/div[1]/div/div/div/div[2]")).click();
        //Thread.sleep(1500);
        //driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[3]")).click();
        //Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[4]")).click();
        Thread.sleep(1500);
    }

    @Test(priority = 3, description = "Export as csv")
    public void exportCSV() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-invoice/div/button[1]")).click();
        Thread.sleep(1500);
    }

    @Test(priority = 4, description = "Edit invoice details")
    public void editInvoice() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-invoice/div/button[3]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/textarea")).clear();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/textarea")).
                sendKeys("Route 13");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/textarea")).clear();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/textarea")).
                sendKeys("Street 34");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[7]/td[2]/input")).clear();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[7]/td[2]/input")).
                sendKeys("20");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[2]")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 5, description = "Generate invoice pdf")
    public void generatePdf() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-invoice/div/button[4]")).click();
        Thread.sleep(1400);
        ArrayList<String> newTb = new ArrayList<String>(driver.getWindowHandles());
        Thread.sleep(2000);
        driver.switchTo().window(newTb.get(0));
    }

}
