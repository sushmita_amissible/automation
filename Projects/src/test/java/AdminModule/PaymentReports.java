package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class PaymentReports {
    WebDriver driver;

    @BeforeTest
    public void test() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority = 1, description = "Login Admin module and open payment reports")
    public void paymentReport() throws InterruptedException {
        //Open website
        driver.get("https://www.amissible.com:444/#/home");
        driver.findElement(By.xpath("//a[@href ='http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/']")).click();

        //Login as Admin
        WebElement email = driver.findElement(By.name("email"));
        Thread.sleep(1500);
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        Thread.sleep(1500);
        WebElement login = driver.findElement(By.name("login"));
        login.click();

        //Open consignment's report
        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();
        Thread.sleep(1500);
        driver.findElement(By.linkText("Payment Report")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 2, description = "Download payment receipt")
    public void receipt() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"ag-51-input\"]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/app-root/app-report-payment/div/button[2]")).click();
        Thread.sleep(1400);
        //driver.findElement(By.xpath("//*[@id=\"download\"]")).click();
        //Thread.sleep(2000);
        ArrayList<String> newTb = new ArrayList<String>(driver.getWindowHandles());
        Thread.sleep(1300);
        driver.switchTo().window(newTb.get(0));
        Thread.sleep(1400);
    }

    @Test(priority = 3, description = "Export employee records as csv")
    public void exportCSV(){
        driver.findElement(By.xpath("/html/body/app-root/app-report-payment/div/button[1]")).click();

    }
}
