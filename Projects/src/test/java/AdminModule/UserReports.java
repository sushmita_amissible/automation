package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class UserReports {

    WebDriver driver;
    @Test(description = "Dealing with user reports")
    public void Test() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Open website
        System.out.println("Hello test ");
        driver.get("http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/");

        //Login as Admin
        WebElement email = driver.findElement(By.name("email"));
        Thread.sleep(1500);
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        Thread.sleep(1500);
        WebElement login = driver.findElement(By.name("login"));
        login.click();

        //Open user's reports
        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();
        Thread.sleep(1500);
        driver.findElement(By.linkText("User Report")).click();

        //Edit user's detail
        driver.findElement(By.xpath("//input[@id ='ag-135-input']")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/app-root/app-report-user/div/button[2]")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/input")).clear();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/input")).sendKeys("Urvashi");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/input")).clear();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/input")).sendKeys("4440989700");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select")).click();
        {
            WebElement dropdown = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select/option[466]"));
            dropdown.findElement(By.xpath("//option[. = 'Europe/Samara']")).click();
        }driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select")).click();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(2000);
        driver.findElement(By.className("close")).click();

        //Export csv file of user's report
        WebElement export = driver.findElement(By.xpath("/html/body/app-root/app-report-user/div/button[1]"));
        Thread.sleep(1500);
        export.click();
    }
}
