package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Admin_login {

    public WebDriver driver;

    @BeforeTest
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test(priority = 1, description = "Login to Admin Module")
    public void login() throws InterruptedException {
        driver.get("https://www.amissible.com:444/#/home");
        Thread.sleep(1600);
        driver.findElement(By.xpath("//a[@href ='http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/']")).click();

        //Enter Login Details & Submit
        driver.findElement(By.name("email")).sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        driver.findElement(By.name("login")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 2, description = "Logout from Admin module")
    public void logout() throws InterruptedException {
        Thread.sleep(2000);
        driver.findElement(By.partialLinkText("Logout")).click();
        Thread.sleep(2000);
        driver.findElement(By.partialLinkText("Main Website")).click();
    }
}