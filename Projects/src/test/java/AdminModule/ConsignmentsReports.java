package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ConsignmentsReports {

    WebDriver driver;

    @BeforeTest
    public void test() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority = 1, description = "Login to admin module and open consignment reports")
    public void  consignment() throws InterruptedException {
        //Open website
        System.out.println("Hello test ");
        driver.get("http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/");

        //Login as Admin
        WebElement email = driver.findElement(By.name("email"));
        Thread.sleep(1500);
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        Thread.sleep(1500);
        WebElement login = driver.findElement(By.name("login"));
        login.click();

        //Open consignment's report
        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();
        Thread.sleep(1500);
        driver.findElement(By.linkText("Consignment Report")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 2, description = "Generate a new task")
    public void newTask() throws InterruptedException{
        //New task
        driver.findElement(By.xpath("/html/body/app-root/app-report-consignment/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[1]/div[1]/div/div/div/div[2]/input")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/app-root/app-report-consignment/div/button[6]")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td/select")).click();
        {
            WebElement dropdown = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td/select/option[3]"));
            dropdown.findElement(By.xpath("//option[. = 'Damon']")).click();
            Thread.sleep(1500);

        } driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td/select")).click();
        Thread.sleep(1500);
        WebElement instructions = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td/textarea"));
        instructions.clear();
        instructions.sendKeys("Packing service");
        Thread.sleep(1500);
        WebElement notes = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[3]/td/textarea"));
        notes.clear();
        notes.sendKeys("Packing service");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div[1]/button/span")).click();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div[1]/button/span")).click();
        Thread.sleep(2000);
        //driver.findElement(By.xpath("/html/body/modal-container")).click();
        driver.findElement(By.xpath("/html/body/app-root/app-report-consignment/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[1]/div[1]/div/div/div/div[2]/input")).click();
        Thread.sleep(2000);

    }


    @Test(priority = 3, description = "Generate new Invoice")
    public void newInvoice() throws InterruptedException {
        //New Invoice
        driver.findElement(By.xpath("//*[@id=\"ag-264-input\"]")).click();
        Thread.sleep(1200);
        driver.findElement(By.xpath("/html/body/app-root/app-report-consignment/div/button[5]")).click();
        //driver.findElement(By
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/modal-container")).click();
        driver.findElement(By.xpath("/html/body/modal-container")).click();
        Thread.sleep(2000);
    }

    @Test(priority = 4, description = "See questionnaires")
    public void questionnaire() throws InterruptedException {
        //driver.findElement(By.xpath("//*[@id=\"ag-264-input\"]")).click();
        Thread.sleep(1200);
        driver.findElement(By.xpath("/html/body/app-root/app-report-consignment/div/button[4]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button")).click();
        Thread.sleep(2000);
    }

   @Test(priority = 5, description = "Generate contract")
    public void contract() throws InterruptedException {
        //driver.findElement(By.xpath("//*[@id=\"ag-264-input\"]")).click();
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/app-root/app-report-consignment/div/button[3]")).click();
        Thread.sleep(1300);
        //Enter Contract Details
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/input")).sendKeys("17-05-2021");
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/textarea")).sendKeys("Statement ____");
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[3]/td[2]/textarea")).sendKeys("First Party ___");
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/textarea")).sendKeys("Second Party ___");
        Thread.sleep(1300);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[5]/td[2]/textarea")).sendKeys("Other Terms Here ___");
        Thread.sleep(1300);
        //Save Contract
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[3]")).click();
        Thread.sleep(1300);
        //Generate Contract
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[2]")).click();
        Thread.sleep(1300);
        ArrayList<String> newTb = new ArrayList<String>(driver.getWindowHandles());
        Thread.sleep(2000);
        driver.switchTo().window(newTb.get(0));
        System.out.println("Page title of parent window: " + driver.getTitle());
        //Signed Contract
        driver.findElement(By.xpath("/html/body/app-root/app-report-consignment/div/button[3]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div[1]/button")).click();
       Thread.sleep(2000);
    }

    @Test(priority = 6, description = "Edit consignment details")
    public void editConsignment() throws InterruptedException {
        //driver.findElement(By.xpath("//*[@id=\"ag-264-input\"]")).click();
        Thread.sleep(1200);
        driver.findElement(By.xpath("/html/body/app-root/app-report-consignment/div/button[2]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[3]/td[2]/textarea")).
                sendKeys("Bell Colony");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/textarea")).
                sendKeys("Street-5");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[5]/td[2]/input")).
                sendKeys("17-04-2021");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[8]/td[2]/select")).click();
        {
            WebElement dropdown = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[8]/td[2]/select/option[4]"));
            dropdown.findElement(By.xpath("//option[. = 'EXECUTING']")).click();
            Thread.sleep(1500);

        }driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[8]/td[2]/select")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[9]/td[2]/textarea")).
                sendKeys("executing");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[2]")).click();
    }

}
