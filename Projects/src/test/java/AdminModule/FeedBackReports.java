package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class FeedBackReports {
    WebDriver driver;

    @BeforeTest
    public void test() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority = 1, description = "Login as Admin and open feedback reports")
    public void feedbackReport() throws InterruptedException {
        //Open website
        driver.get("https://www.amissible.com:444/#/home");
        driver.findElement(By.xpath("//a[@href ='http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/']")).click();

        //Login as Admin
        WebElement email = driver.findElement(By.name("email"));
        Thread.sleep(1500);
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        Thread.sleep(1500);
        WebElement login = driver.findElement(By.name("login"));
        login.click();

        //Open consignment's report
        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();
        Thread.sleep(1500);
        driver.findElement(By.linkText("Feedback Report")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 2, description = "export csv report")
    public void exportCsv() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-feedback/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/app-root/app-report-feedback/div/button")).click();
    }

}
