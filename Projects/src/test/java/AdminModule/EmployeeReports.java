package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class EmployeeReports {
    WebDriver driver;

    @BeforeTest
    public void test() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority = 1, description = "Login and open Invoice Reports")
    public void employeeReport() throws InterruptedException {
        //Open website
        driver.get("https://www.amissible.com:444/#/home");
        driver.findElement(By.xpath("//a[@href ='http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/']")).click();

        //Login as Admin
        WebElement email = driver.findElement(By.name("email"));
        Thread.sleep(1500);
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        Thread.sleep(1500);
        WebElement login = driver.findElement(By.name("login"));
        login.click();

        //Open consignment's report
        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();
        Thread.sleep(1500);
        driver.findElement(By.linkText("Employee Report")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/app-root/app-menu/div")).click();
    }

    @Test(priority = 2, description = "Add a new Employee")
    public void newEmployee() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-employee/div/button[3]")).click();
        Thread.sleep(1500);
        //enter details
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td/input")).sendKeys("Henry");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td/input")).sendKeys("henry@gmail.com");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[3]/td/input")).sendKeys("9119093450");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td/input")).sendKeys("Mary");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[5]/td/select")).click();
        {
            WebElement dropdown = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[5]/td/select/option[2]"));
            dropdown.findElement(By.xpath("//option[. = 'EMPLOYEE']")).click();
            Thread.sleep(1500);

        }driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[5]/td/select")).click();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1500);

        driver.findElement(By.xpath("/html/body/modal-container/div/div/div[1]/button")).click();
    }


    @Test(priority = 3,description = "Edit employees detail")
    public void edit() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-employee/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[5]/div[1]/div/div/div/div[2]")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/app-root/app-report-employee/div/button[2]")).click();
        //Enter Details
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/input")).clear();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/input")).
                sendKeys("Anna");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/input")).clear();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/input")).
                sendKeys("7698578690");
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select")).click();
        {
            WebElement dropdown = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select/option[2]"));
            dropdown.findElement(By.xpath("//option[. = 'EMPLOYEE']")).click();
            Thread.sleep(1500);

        }driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select")).click();
        Thread.sleep(1200);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[2]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/app-root/app-report-employee/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[5]/div[1]/div/div/div/div[2]")).click();
    }

    @Test(priority = 4,description = "Export Employees CSV record")
    public void exportCSV() throws InterruptedException {
        Thread.sleep(1500);
        driver.findElement(By.xpath("/html/body/app-root/app-report-employee/div/button[1]")).click();
    }
}
