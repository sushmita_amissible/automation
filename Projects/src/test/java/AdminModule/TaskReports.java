package AdminModule;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class TaskReports {
    WebDriver driver;

    @BeforeTest
    public void test() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test(priority = 1, description = "Login to admin module and open task reports")
    public void  consignment() throws InterruptedException {
        //Open website
        System.out.println("Hello test ");
        driver.get("http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/");

        //Login as Admin
        WebElement email = driver.findElement(By.name("email"));
        Thread.sleep(1500);
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");
        Thread.sleep(1500);
        WebElement login = driver.findElement(By.name("login"));
        login.click();

        //Open consignment's report
        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();
        Thread.sleep(1500);
        driver.findElement(By.linkText("Task Report")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 2,description = "Delete a task")
    public void deleteTask() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-task/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[4]/div[1]/div/div/div/div[2]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/app-root/app-report-task/div/button[3]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div[1]/button")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 3, description = "Edit a Task report")
    public void editTask() throws InterruptedException {
        driver.findElement(By.xpath("/html/body/app-root/app-report-task/ag-grid-angular/div/div[1]/div[2]/div[3]/div[2]/div/div/div[7]/div[1]/div/div/div/div[2]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/app-root/app-report-task/div/button[2]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/select")).click();
        {
            WebElement e = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/select/option[4]"));
            e.findElement(By.xpath("//option[. = 'Anna']")).click();
            Thread.sleep(1400);

        }driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/select")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/textarea")).clear();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/textarea")).
                sendKeys("Moving to pack");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[3]/td[2]/textarea")).clear();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[3]/td[2]/textarea")).
                sendKeys("Moving to somewhere....");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select")).click();
        {
            WebElement element = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select/option[2]"));
            element.findElement(By.xpath("//option[. = 'EXECUTING']")).click();
            Thread.sleep(1400);

        }driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select")).click();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[6]/td[2]/input")).
                sendKeys("20-05-2021");
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[2]")).click();
        Thread.sleep(1400);
    }

    @Test(priority = 4, description = "export task report in csv file")
    public void exportCSV() throws InterruptedException {
        Thread.sleep(1400);
        driver.findElement(By.xpath("/html/body/app-root/app-report-task/div/button[1]")).click();
    }
}
