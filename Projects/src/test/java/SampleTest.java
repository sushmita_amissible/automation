import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SampleTest {

    WebDriver driver;
    @Test
    public void Test() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        driver.manage().deleteAllCookies();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        System.out.println("Hello test ");
        driver.get("http://moving-solution-management-ui.s3-website.ap-south-1.amazonaws.com/");

        WebElement email = driver.findElement(By.name("email"));
        email.sendKeys("omendra@gmail.com");
        driver.findElement(By.name("password")).sendKeys("123");

        WebElement login = driver.findElement(By.name("login"));
        login.click();

        WebElement report = driver.findElement(By.className("arrow-down"));
        report.click();

        driver.findElement(By.linkText("User Report")).click();
        driver.findElement(By.xpath("//input[@id ='ag-135-input']")).click();
        driver.findElement(By.xpath("/html/body/app-root/app-report-user/div/button[2]")).click();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/input")).clear();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[1]/td[2]/input")).sendKeys("Urvashi");
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/input")).clear();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[2]/td[2]/input")).sendKeys("4440989700");
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select")).click();
        {
            WebElement dropdown = driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select/option[466]"));
            dropdown.findElement(By.xpath("//option[. = 'Europe/Samara']")).click();
        }driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[2]/table/tbody/tr[4]/td[2]/select")).click();
        driver.findElement(By.xpath("/html/body/modal-container/div/div/div/div[1]/div/button[1]")).click();
        Thread.sleep(2000);
        driver.findElement(By.className("close")).click();
    }
}
